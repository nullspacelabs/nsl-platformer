﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
    public float JumpOffset = 1f;
    public float DesiredSpeed;
    private Rigidbody2D body;

	// Use this for initialization
	void Awake () {
        body = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        Debug.DrawLine(transform.position, transform.position + Vector3.down * JumpOffset);
        DesiredSpeed = 0f;
        if (Input.GetKey(KeyCode.RightArrow))
        {
            DesiredSpeed = 5f;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            DesiredSpeed = -5f;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.up, JumpOffset);
            //Debug.Log(LayerMask.NameToLayer("Terrain"));
            //Debug.Log(hit.collider);
            if (hit.collider != null )
            {
                GetComponent<Rigidbody2D>().AddForce(Vector2.up * 6, ForceMode2D.Impulse);
            }
            
        }
	}

    void FixedUpdate()
    {
        var adjustedspeed = new Vector3(DesiredSpeed - body.velocity.x, 0)*0.1f;
        Debug.DrawLine(transform.position, transform.position + adjustedspeed);
        body.AddForce(adjustedspeed, ForceMode2D.Impulse);
    }
}
