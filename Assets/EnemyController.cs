﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {
    public int WalkDirection = -1;
    public float WalkSpeed = 10f;
    public Vector3 CliffDetector = new Vector3(-1, -0.25f, 0);
    private Rigidbody2D body;

	// Use this for initialization
	void Awake () {
        body = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        Debug.Log(body.velocity.x);
        if (Mathf.Abs(body.velocity.x) < WalkSpeed)
        {
            body.AddForce(Vector2.right * WalkDirection * WalkSpeed*0.1f,ForceMode2D.Impulse);
        }
        RaycastHit2D hit = Physics2D.Raycast(transform.position, CliffDetector, 1f);
        if (hit.collider != null)
        {} else {
            WalkDirection *= -1;
            CliffDetector = Vector2.Scale(CliffDetector, new Vector2(-1,1));
            var scale = transform.localScale;
            transform.localScale = new Vector3(scale.x*-1, scale.y,scale.z);

        }
        Debug.DrawLine(transform.position, transform.position + CliffDetector);

	}
}
